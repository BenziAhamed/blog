# Why

The reason is simple: usually when you make a statement or give an opinion, you should have arguments to defend it and make your readers understand that you are not just writing/talking garbage. If you do not do that every time you give an information or opinion, then you will lose credibility and a sense of culture.

I personally would not believe in someone who gives a statement without argument when requested.

If you want contact me on Threema: https://three.ma/736WU8VV or in XMPP (only with OMEMO encrytion): edu4rdshl@conversations.im or follow me on Twitter: https://twitter.com/edu4rdshl